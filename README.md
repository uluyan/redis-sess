# redis-sess

A redis-based session store.

## Usage

```js
const RedisSession = require('redis-sess');

run()
    .then(() => console.log('ok'))
    .catch(console.error);

async function run() {
    const rs = new RedisSession();

    // create a session
    const sessionId = await rs.createSession({
        name: 'redis-sess'
    });

    // get meta by session id
    const meta = await rs.getSession(sessionId);
    console.log(meta);

    // update session
    await rs.updateSession(sessionId);

    // handle error
    rs.on('error', console.error);
}
```

## Configuration

```js
const RedisSession = require('redis-sess');

const rs = new RedisSession({
    redis: {
        port: 6379,
        host: '127.0.0.1',
    },
    liveTime: 100, // if liveTime is 0, the session will never be expired
    prefix: 'my-session',
});

// or
const rs1 = new RedisSession('redis://127.0.0.1:6379');

// or
const rs2 = new RedisSession(300);
```
