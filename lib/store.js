'use strict';

const debug = require('debug')('redis-sess');
const Redis = require('ioredis');
const uid = require('uid-safe');
const util = require('util');

module.exports = class RedisSessionStore {
    constructor(option) {
        const config = {
            liveTime: 600,
            redis: 'redis://127.0.0.1:6379',
            prefix: 'session',
        };

        // configuration
        if (util.isNumber(option)) {
            config.liveTime = option;
        } else if (util.isString(option)) {
            config.redis = option;
        } else {
            Object.assign(config, option);
        }

        this.redis = new Redis(config.redis);
        this.liveTime = config.liveTime;
        this.prefix = config.prefix;
    }

    async getSessionId() {
        const id = await uid(24);
        return `${this.prefix}-${id}`;
    }

    async saveSession(sessionId, meta, json = true) {
        if (util.isUndefined(sessionId)) return false;
        if (util.isUndefined(meta)) return false;
        const value = json ? JSON.stringify(meta) : meta;
        await this.redis.set(sessionId, value);
        return true;
    }

    async getSession(sessionId, json = true) {
        if (util.isUndefined(sessionId)) return null;
        const meta = await this.redis.get(sessionId);
        return RedisSessionStore.parseJSON(meta);
    }

    async expireSession(sessionId) {
        if (util.isUndefined(sessionId)) return false;
        if (this.liveTime === 0) return false;
        const expireAt = parseInt(Date.now() / 1000) + this.liveTime;
        const res = await this.redis.expireat(sessionId, expireAt);
        return res > 0;
    }

    static parseJSON(str) {
        if (!util.isString(str)) return {};
        try {
            return JSON.parse(str);
        } catch (err) {
            debug('%s is not correct json string', str);
            return {};
        }
    }

    listSessions() {
        return this.redis.keys(`${this.prefix}-*`);
    }

    ttlSession(sessionId) {
        return this.redis.ttl(sessionId);
    }
};
