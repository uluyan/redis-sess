'use strict';

const Promise = require('bluebird');
const { EventEmitter } = require('events');
const RedisSessionStore = require('./lib/store');

const store = Symbol('redis-session-store');

module.exports = class RedisSession extends EventEmitter {
    constructor(...args) {
        super();
        this[store] = new RedisSessionStore(...args);
        this[store].redis.on('error', (err) => this.silentEmit('error', err));
    }

    /**
     * Emit only when there's at least one listener.
     *
     * @param {string} eventName
     * @param {*} error
     * @return {boolean}
     * @private
     */
    silentEmit(eventName, error) {
        if (this.listeners(eventName).length > 0) {
            return this.emit(eventName, error);
        }
        if (error && error instanceof Error) {
            console.error('[redis-sess] Unhandled error event:', error.stack);
        }
        return false;
    };

    /**
     * Create Session
     * @param {*} meta
     * @return {Promise<boolean>}
     */
    async createSession(meta) {
        const sessionId = await this[store].getSessionId();
        await this[store].saveSession(sessionId, meta);
        await this[store].expireSession(sessionId);
        return sessionId;
    }

    /**
     * Get Session by session id
     * @param {string} sessionId
     * @return {Promise<any>}
     */
    async getSession(sessionId) {
        const meta = await this[store].getSession(sessionId);
        meta.ttl = await this[store].ttlSession(sessionId);
        return meta;
    }

    /**
     * Update Session meta and time to live
     * @param {string} sessionId
     * @param {*} meta
     * @param {boolean} json
     * @return {Promise<boolean>}
     */
    async updateSession(sessionId, meta, json) {
        const res = await Promise.all([
            this[store].saveSession(sessionId, meta, json),
            this[store].expireSession(sessionId),
        ]);
        return res[1];
    }

    /**
     * List Sessions
     * @return {Promise<string[]>}
     */
    listSessions() {
        return this[store].listSessions();
    }
};
